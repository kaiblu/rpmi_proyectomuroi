﻿using UnityEngine;
using UnityEngine.UI;
using System;
using Ink.Runtime;
using System.Collections.Generic;
using TMPro;
using UnityEngine.Video;
using UnityEngine.SceneManagement; 

// This is a super bare bones example of how to play and display a ink story in Unity.
public class BasicInkExample : MonoBehaviour {
    public static event Action<Story> OnCreateStory;

    public static bool GameIsPaused = false;

    public GameObject pauseMenuUI;

    private const string SPEAKER_TAG = "speaker";
    private const string PORTRAIT_TAG = "portrait";
    private const string BG_TAG = "BG";
    private const string VIDEO_TAG = "Video";
    private const string INTERFAZ_TAG = "Interfaz";
    private const string ESCENA_TAG = "Escena";
    private const string AUDIO_TAG = "audio";
    private const string REVISTA_TAG = "Revista";

    [SerializeField] private TextMeshProUGUI displayNameText;
    [SerializeField] private Animator BG;
    [SerializeField] private Animator ZaidaNormal;
    [SerializeField] private Animator Khadija;
    [SerializeField] private Animator Haro;
    [SerializeField] private Animator Rajoy;
    [SerializeField] private Animator Walid;
    [SerializeField] private Animator Abuelo;
    [SerializeField] private Animator Izai;
    [SerializeField] private Animator PAP;
    [SerializeField] private Animator Todos;
    [SerializeField] private Animator Akil;
    [SerializeField] private Animator Andres;
    [SerializeField] private Animator Piedad;
    [SerializeField] private Animator Revista;


    [SerializeField] private GameObject ZaidaaNormal;
    [SerializeField] private GameObject Khadijaa;
    [SerializeField] private GameObject Haroo;
    [SerializeField] private GameObject Rajoyy;
    [SerializeField] private GameObject Walidd;
    [SerializeField] private GameObject Abueloo;
    [SerializeField] private GameObject Izaii;
    [SerializeField] private GameObject PAPP;
    [SerializeField] private GameObject Todoss;
    [SerializeField] private GameObject AniHH;
    [SerializeField] private GameObject AniWW;
    [SerializeField] private GameObject Copening;
    [SerializeField] private GameObject Akill;
    [SerializeField] private GameObject Andress;
    [SerializeField] private GameObject Piedadd;
    [SerializeField] private GameObject Carta;
    [SerializeField] private GameObject Revista1a;
    [SerializeField] private GameObject Revista1b;
    [SerializeField] private GameObject Revista1c;
    [SerializeField] private GameObject Revista1d;
    [SerializeField] private GameObject Revista1e;
    [SerializeField] private GameObject Revista1f;
    [SerializeField] private GameObject Revista0;
    [SerializeField] private GameObject Dibujo;
    [SerializeField] private GameObject Dibujo2;
    [SerializeField] private GameObject Diario;
    [SerializeField] private GameObject Diario2;
    [SerializeField] private GameObject Revistaa;
    [SerializeField] private GameObject Revista2a;
    [SerializeField] private GameObject Revista2b;
    [SerializeField] private GameObject Revista2c;
    [SerializeField] private GameObject Revista2d;
    [SerializeField] private GameObject Revista2e;
    [SerializeField] private GameObject Revista2f;
    [SerializeField] private GameObject Revista2g;
    [SerializeField] private GameObject AniII;
    [SerializeField] private GameObject AniCuartoo;
    [SerializeField] private GameObject AniBiblioo;
    [SerializeField] private GameObject Novioos;
    [SerializeField] private GameObject Investii;
    [SerializeField] private GameObject Creditoos;
    public VideoPlayer aniH;
    public VideoPlayer aniW;
    public VideoPlayer opening;
    public VideoPlayer aniI;
    public VideoPlayer anicuarto;
    public VideoPlayer anibiblio;
    public VideoPlayer aninovios;
    public VideoPlayer aniinvesti;
    public VideoPlayer anicreditos;

    public AudioSource Blackpink;
    public AudioSource Neutral;
    public AudioSource Gogo;
    public AudioSource Triste;
    public AudioSource Null;
    public AudioSource Neutral2;
    public AudioSource Acción;
    public AudioSource Miedo;
    public AudioSource Noche;
    public AudioSource Feliz;
    public AudioSource Tensión;


    void Awake () {
		// Remove the default message
		RemoveChildren();
		StartStory();
	}

	// Creates a new Story object with the compiled story which we can then play!
	void StartStory () {
		story = new Story (inkJSONAsset.text);
        if(OnCreateStory != null) OnCreateStory(story);
		RefreshView();
	}

    // This is the main function called every time the story changes. It does a few things:
    // Destroys all the old content and choices.
    // Continues over all the lines of text, then displays all the choices. If there are no choices, the story is finished!

    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            RefreshView();

        }


        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("pausa");
            if (GameIsPaused)
            {

                Resume();

            }
            else
            {

                Paused();

            }

        }

    }

    public void Resume()
    {

        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;

    }

    void Paused()
    {

        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;

    }

    public void LoadMenu()
    {

        SceneManager.LoadScene("Menu");

    }

    public void QuitGame()
    {

        Application.Quit();

    }

    void RefreshView () {
		// Remove all the UI on screen
		RemoveChildren ();
		
		// Read all the content until we can't continue any more
		if (story.canContinue) {
			// Continue gets the next line of the story
			string text = story.Continue ();
			// This removes any white space from the text.
			text = text.Trim();
			// Display the text on screen!
			CreateContentView(text);

            HandleTags(story.currentTags);

		}
        else if(story.currentChoices.Count > 0) {
            // Display all the choices, if there are any!
            for (int i = 0; i < story.currentChoices.Count; i++) {
                //Debug.Log("furula2");
				Choice choice = story.currentChoices [i];
				Button button = CreateChoiceView (choice.text.Trim ());
				// Tell the button what to do when we press it
				button.onClick.AddListener (()=> {
                    //Debug.Log("furula3");
					OnClickChoiceButton (choice);
				});
			}
		}

        
		// If we've read all the content and there's no choices, the story is finished!
		//else {
		//	Button choice = CreateChoiceView("End of story.\nRestart?");
		//	choice.onClick.AddListener(delegate{
		//		StartStory();
		//	});
		//}
	}
    private void HandleTags(List<string> currentTags)
    {

        foreach (string tag in currentTags)
        {

            string[] splitTag = tag.Split(':');
            if(splitTag.Length != 2)
            {
                Debug.LogError("Tag no furula: " + tag);
            }
            string tagKey = splitTag[0].Trim();
            string tagValue = splitTag[1].Trim();

            switch (tagKey)
            {

                case SPEAKER_TAG:
                    //Debug.Log("speaker=" + tagValue);
                    displayNameText.text = tagValue;
                    
                    switch (tagValue)
                    {
                        case "Zaida":
                            ZaidaaNormal.SetActive(true);
                            Khadijaa.SetActive(false);
                            Rajoyy.SetActive(false);
                            Haroo.SetActive(false);
                            Abueloo.SetActive(false);
                            Walidd.SetActive(false);
                            Izaii.SetActive(false);
                            PAPP.SetActive(false);
                            Todoss.SetActive(false);
                            Piedadd.SetActive(false);
                            Andress.SetActive(false);
                            Akill.SetActive(false);
                            break;
                        case "Khadija":
                            Khadijaa.SetActive(true);
                            ZaidaaNormal.SetActive(false);
                            Rajoyy.SetActive(false);
                            Haroo.SetActive(false);
                            Abueloo.SetActive(false);
                            Walidd.SetActive(false);
                            Izaii.SetActive(false);
                            PAPP.SetActive(false);
                            Todoss.SetActive(false);
                            Piedadd.SetActive(false);
                            Andress.SetActive(false);
                            Akill.SetActive(false);
                            break;
                        case "Haroldo":
                            ZaidaaNormal.SetActive(false);
                            Khadijaa.SetActive(false);
                            Haroo.SetActive(true);
                            Rajoyy.SetActive(false);
                            Abueloo.SetActive(false);
                            Walidd.SetActive(false);
                            Izaii.SetActive(false);
                            PAPP.SetActive(false);
                            Todoss.SetActive(false);
                            Piedadd.SetActive(false);
                            Andress.SetActive(false);
                            Akill.SetActive(false);
                            break;
                        case "Haro":
                            ZaidaaNormal.SetActive(false);
                            Khadijaa.SetActive(false);
                            Haroo.SetActive(true);
                            Rajoyy.SetActive(false);
                            Abueloo.SetActive(false);
                            Walidd.SetActive(false);
                            Izaii.SetActive(false);
                            PAPP.SetActive(false);
                            Todoss.SetActive(false);
                            Piedadd.SetActive(false);
                            Andress.SetActive(false);
                            Akill.SetActive(false);

                            break;
                        case "M.Rajoy":
                            ZaidaaNormal.SetActive(false);
                            Khadijaa.SetActive(false);
                            Haroo.SetActive(false);
                            Rajoyy.SetActive(true);
                            Abueloo.SetActive(false);
                            Walidd.SetActive(false);
                            Izaii.SetActive(false);
                            PAPP.SetActive(false);
                            Todoss.SetActive(false);
                            Piedadd.SetActive(false);
                            Andress.SetActive(false);
                            Akill.SetActive(false);
                            break;
                        case "Walid":
                            ZaidaaNormal.SetActive(false);
                            Khadijaa.SetActive(false);
                            Haroo.SetActive(false);
                            Rajoyy.SetActive(false);
                            Walidd.SetActive(true);
                            Abueloo.SetActive(false);
                            Izaii.SetActive(false);
                            PAPP.SetActive(false);
                            Todoss.SetActive(false);
                            Piedadd.SetActive(false);
                            Andress.SetActive(false);
                            Akill.SetActive(false);
                            break;
                        case "Abuelo":
                            ZaidaaNormal.SetActive(false);
                            Khadijaa.SetActive(false);
                            Haroo.SetActive(false);
                            Rajoyy.SetActive(false);
                            Walidd.SetActive(false);
                            Abueloo.SetActive(true);
                            Izaii.SetActive(false);
                            PAPP.SetActive(false);
                            Todoss.SetActive(false);
                            Piedadd.SetActive(false);
                            Andress.SetActive(false);
                            Akill.SetActive(false);
                            break;
                        case "Izai":
                            ZaidaaNormal.SetActive(false);
                            Khadijaa.SetActive(false);
                            Haroo.SetActive(false);
                            Rajoyy.SetActive(false);
                            Walidd.SetActive(false);
                            Abueloo.SetActive(false);
                            Izaii.SetActive(true);
                            PAPP.SetActive(false);
                            Todoss.SetActive(false);
                            Piedadd.SetActive(false);
                            Andress.SetActive(false);
                            Akill.SetActive(false);
                            break;
                        case "Patrick":
                            ZaidaaNormal.SetActive(false);
                            Khadijaa.SetActive(false);
                            Haroo.SetActive(false);
                            Rajoyy.SetActive(false);
                            Walidd.SetActive(false);
                            Abueloo.SetActive(false);
                            Izaii.SetActive(false);
                            PAPP.SetActive(true);
                            Todoss.SetActive(false);
                            Piedadd.SetActive(false);
                            Andress.SetActive(false);
                            Akill.SetActive(false);
                            break;
                        case "Acfred":
                            ZaidaaNormal.SetActive(false);
                            Khadijaa.SetActive(false);
                            Haroo.SetActive(false);
                            Rajoyy.SetActive(false);
                            Walidd.SetActive(false);
                            Abueloo.SetActive(false);
                            Izaii.SetActive(false);
                            PAPP.SetActive(true);
                            Todoss.SetActive(false);
                            Piedadd.SetActive(false);
                            Andress.SetActive(false);
                            Akill.SetActive(false);
                            break;
                        case "Profesora":
                            ZaidaaNormal.SetActive(false);
                            Khadijaa.SetActive(false);
                            Haroo.SetActive(false);
                            Rajoyy.SetActive(false);
                            Walidd.SetActive(false);
                            Abueloo.SetActive(false);
                            Izaii.SetActive(false);
                            PAPP.SetActive(true);
                            Todoss.SetActive(false);
                            Piedadd.SetActive(false);
                            Andress.SetActive(false);
                            Akill.SetActive(false);
                            break;
                        case "Todos":
                            ZaidaaNormal.SetActive(false);
                            Khadijaa.SetActive(false);
                            Haroo.SetActive(false);
                            Rajoyy.SetActive(false);
                            Walidd.SetActive(false);
                            Abueloo.SetActive(false);
                            Izaii.SetActive(false);
                            PAPP.SetActive(false);
                            Todoss.SetActive(true);
                            Piedadd.SetActive(false);
                            Andress.SetActive(false);
                            Akill.SetActive(false);
                            break;

                        case "Akil":
                            ZaidaaNormal.SetActive(false);
                            Khadijaa.SetActive(false);
                            Haroo.SetActive(false);
                            Rajoyy.SetActive(false);
                            Walidd.SetActive(false);
                            Abueloo.SetActive(false);
                            Izaii.SetActive(false);
                            PAPP.SetActive(false);
                            Todoss.SetActive(false);
                            Piedadd.SetActive(false);
                            Andress.SetActive(false);
                            Akill.SetActive(true);
                            break;

                        case "Don Andrés":
                            ZaidaaNormal.SetActive(false);
                            Khadijaa.SetActive(false);
                            Haroo.SetActive(false);
                            Rajoyy.SetActive(false);
                            Walidd.SetActive(false);
                            Abueloo.SetActive(false);
                            Izaii.SetActive(false);
                            PAPP.SetActive(false);
                            Todoss.SetActive(false);
                            Piedadd.SetActive(false);
                            Andress.SetActive(true);
                            Akill.SetActive(false);
                            break;

                        case "Doña Piedad":
                            ZaidaaNormal.SetActive(false);
                            Khadijaa.SetActive(false);
                            Haroo.SetActive(false);
                            Rajoyy.SetActive(false);
                            Walidd.SetActive(false);
                            Abueloo.SetActive(false);
                            Izaii.SetActive(false);
                            PAPP.SetActive(false);
                            Todoss.SetActive(false);
                            Piedadd.SetActive(true);
                            Andress.SetActive(false);
                            Akill.SetActive(false);
                            break;

                    }
                    break;

                case PORTRAIT_TAG:
                    ZaidaNormal.Play(tagValue);
                    Khadija.Play(tagValue);
                    Haro.Play(tagValue);
                    Rajoy.Play(tagValue);
                    Walid.Play(tagValue);
                    Abuelo.Play(tagValue); 
                    Izai.Play(tagValue);
                    PAP.Play(tagValue);
                    Todos.Play(tagValue);
                    Akil.Play(tagValue);
                    Piedad.Play(tagValue);
                    Andres.Play(tagValue);
                    switch(tagValue)
                    {

                        case "Todos":
                            Todos.Play(tagValue);
                            ZaidaaNormal.SetActive(false);
                            Khadijaa.SetActive(false);
                            Haroo.SetActive(false);
                            Rajoyy.SetActive(false);
                            Walidd.SetActive(false);
                            Abueloo.SetActive(false);
                            Izaii.SetActive(false);
                            PAPP.SetActive(false);
                            Todoss.SetActive(true);
                            Piedadd.SetActive(false);
                            Andress.SetActive(false);
                            Akill.SetActive(false);

                            break;

                    }
                

                    break;

                case BG_TAG:
                    //Debug.Log("BG=" + tagValue);
                    BG.Play(tagValue);
                    break;

                case VIDEO_TAG:
                    //Debug.Log("video2");
                    switch (tagValue)
                    {

                        case "Haro":
                            //Debug.Log("video");
                            AniHH.SetActive(true);
                            aniH.enabled = true;
                            break;

                        case "Walid":
                            //Debug.Log("video3");
                            AniWW.SetActive(true);
                            aniW.enabled = true;
                            break;

                        case "Opening":
                            //Debug.Log("video4");
                            Copening.SetActive(true);
                            opening.enabled = true;
                            break;

                        case "Izai":
                            AniII.SetActive(true);
                            aniI.enabled = true;
                            break;

                        case "Cuarto":
                            AniCuartoo.SetActive(true);
                            anicuarto.enabled = true;
                            break;

                        case "Biblio":
                            AniBiblioo.SetActive(true);
                            anibiblio.enabled = true;
                            break;

                        case "Novios":
                            Novioos.SetActive(true);
                            aninovios.enabled = true;
                            break;

                        case "Investigación":
                            Investii.SetActive(true);
                            aniinvesti.enabled = true;
                            break;

                        case "Epilogo":
                            Creditoos.SetActive(true);
                            anicreditos.enabled = true;
                            break;

                    }
                    break;

                case INTERFAZ_TAG:
                    switch (tagValue)
                    {

                        case "Carta":
                            Carta.SetActive(true);
                            break;

                        case "Revista1a":
                            Revista1a.SetActive(true);
                            break;
                        case "Revista1b":
                            Revista1b.SetActive(true);
                            break;
                        case "Revista1c":
                            Revista1c.SetActive(true);
                            break;
                       case "Revista1d":
                            Revista1d.SetActive(true);
                            break;
                        case "Revista1e":
                            Revista1e.SetActive(true);
                            break;
                        case "Revista1f":
                            Revista1f.SetActive(true);
                            break;
                        case "Revista0":
                            Revista0.SetActive(true);
                            break;
                        case "Dibujo":
                            Dibujo.SetActive(true);
                            break;
                        case "Dibujo2":
                            Dibujo2.SetActive(true);
                            break;
                        case "Diario":
                            Diario.SetActive(true);
                            break;
                        case "Diario2":
                            Diario2.SetActive(true);
                            break;

                        case "Revista2a":
                            Revista2a.SetActive(true);
                            break;
                        case "Revista2b":
                            Revista2b.SetActive(true);
                            break;
                        case "Revista2c":
                            Revista2c.SetActive(true);
                            break;
                        case "Revista2d":
                            Revista2d.SetActive(true);
                            break;
                        case "Revista2e":
                            Revista2e.SetActive(true);
                            break;
                        case "Revista2f":
                            Revista2f.SetActive(true);
                            break;
                        case "Revista2g":
                            Revista2g.SetActive(true);
                            break;




                            //case "Revista0":
                            //    Revista.SetActive(false);
                            //    break;
                    }
                    break;
                case ESCENA_TAG:
                    Debug.Log("Escena=" + tagValue);

                    switch (tagValue)
                    {

                        case "1Parte":
                            //Debug.Log("escena");
                            SceneManager.LoadScene("1Parte");
                            break;

                        case "2Parte":
                            Debug.Log("2");
                           SceneManager.LoadScene("2Parte");
                           break;

                        case "3Parte":
                            SceneManager.LoadScene("3Parte");
                            break;

                        case "BAD":
                            Debug.Log("BAD");
                            SceneManager.LoadScene("BAD");
                            break;
                        case "GOOD":
                            Debug.Log("GOOD");
                            SceneManager.LoadScene("GOOD");
                            break;

                        case "Menu":
                            Debug.Log("Menú");
                            SceneManager.LoadScene("Menu");
                            break;


                    }

                    break;

                case REVISTA_TAG:
                    switch (tagValue)
                    {

                        case "Revista1a":
                            Revista1a.SetActive(true);
                            break;
                        case "Revista1b":
                            Revista1b.SetActive(true);
                            break;
                        case "Revista1c":
                            Revista1c.SetActive(true);
                            break;
                        case "Revista1d":
                            Revista1d.SetActive(true);
                            break;
                        case "Revista1e":
                            Revista1e.SetActive(true);
                            break;
                        case "Revista1f":
                            Revista1f.SetActive(true);
                            break;
                        case "Revista0":
                            Revista0.SetActive(true);
                            break;

                        case "Revista2a":
                            Revista2a.SetActive(true);
                            break;
                        case "Revista2b":
                            Revista2b.SetActive(true);
                            break;
                        case "Revista2c":
                            Revista2c.SetActive(true);
                            break;
                        case "Revista2d":
                            Revista2d.SetActive(true);
                            break;
                        case "Revista2e":
                            Revista2e.SetActive(true);
                            break;
                        case "Revista2f":
                            Revista2f.SetActive(true);
                            break;
                        case "Revista2g":
                            Revista2g.SetActive(true);
                            break;


                    }
                    break;
                     

                case AUDIO_TAG:
                    switch (tagValue)
                    {


                        case "Blackpink":
                            Blackpink.Play();
                            Neutral.Stop();
                            Null.Stop();
                            Triste.Stop();
                            Gogo.Stop();
                            Neutral2.Stop();
                            Tensión.Stop();
                            Feliz.Stop();
                            Acción.Stop();
                            Noche.Stop();
                            Miedo.Stop();
                            break;

                        case "Neutral":
                            Blackpink.Stop();
                            Neutral.Play();
                            Null.Stop();
                            Triste.Stop();
                            Gogo.Stop();
                            Neutral2.Stop();
                            Tensión.Stop();
                            Feliz.Stop();
                            Acción.Stop();
                            Noche.Stop();
                            Miedo.Stop();
                            break;

                        case "Null":
                            Blackpink.Stop();
                            Neutral.Stop();
                            Null.Play();
                            Triste.Stop();
                            Gogo.Stop();
                            Neutral2.Stop();
                            Tensión.Stop();
                            Feliz.Stop();
                            Acción.Stop();
                            Noche.Stop();
                            Miedo.Stop();
                            break;

                        case "Triste":
                            Blackpink.Stop();
                            Neutral.Stop();
                            Null.Stop();
                            Triste.Play();
                            Gogo.Stop();
                            Neutral2.Stop();
                            Tensión.Stop();
                            Feliz.Stop();
                            Acción.Stop();
                            Noche.Stop();
                            Miedo.Stop();
                            break;

                        case "Gogo":
                            Blackpink.Stop();
                            Neutral.Stop();
                            Null.Stop();
                            Triste.Stop();
                            Gogo.Play();
                            Neutral2.Stop();
                            Tensión.Stop();
                            Feliz.Stop();
                            Acción.Stop();
                            Noche.Stop();
                            Miedo.Stop();
                            break;

                        case "Neutral2":
                            Blackpink.Stop();
                            Neutral.Stop();
                            Null.Stop();
                            Triste.Stop();
                            Gogo.Stop();
                            Neutral2.Play();
                            Tensión.Stop();
                            Feliz.Stop();
                            Acción.Stop();
                            Noche.Stop();
                            Miedo.Stop();
                            break;

                        case "Tension":
                            Blackpink.Stop();
                            Neutral.Stop();
                            Null.Stop();
                            Triste.Stop();
                            Gogo.Stop();
                            Neutral2.Stop();
                            Tensión.Play();
                            Feliz.Stop();
                            Acción.Stop();
                            Noche.Stop();
                            Miedo.Stop();
                            break;

                        case "Feliz":
                            Blackpink.Stop();
                            Neutral.Stop();
                            Null.Stop();
                            Triste.Stop();
                            Gogo.Stop();
                            Neutral2.Stop();
                            Tensión.Stop();
                            Feliz.Play();
                            Acción.Stop();
                            Noche.Stop();
                            Miedo.Stop();
                            break;

                        case "Acción":
                            Blackpink.Stop();
                            Neutral.Stop();
                            Null.Stop();
                            Triste.Stop();
                            Gogo.Stop();
                            Neutral2.Stop();
                            Tensión.Stop();
                            Feliz.Stop();
                            Acción.Play();
                            Noche.Stop();
                            Miedo.Stop();
                            break;

                        case "Noche":
                            Blackpink.Stop();
                            Neutral.Stop();
                            Null.Stop();
                            Triste.Stop();
                            Gogo.Stop();
                            Neutral2.Stop();
                            Tensión.Stop();
                            Feliz.Stop();
                            Acción.Stop();
                            Noche.Play();
                            Miedo.Stop();
                            break;

                        case "Miedo":
                            Blackpink.Stop();
                            Neutral.Stop();
                            Null.Stop();
                            Triste.Stop();
                            Gogo.Stop();
                            Neutral2.Stop();
                            Tensión.Stop();
                            Feliz.Stop();
                            Acción.Stop();
                            Noche.Stop();
                            Miedo.Play();
                            break;
                    }

               
                    break;

                default:
                    Debug.LogWarning("Tag no furula parte 2: " + tag);
                    break;
            }

        }

    }
    // When we click the choice button, tell the story to choose that choice!
    void OnClickChoiceButton (Choice choice) {
        //Debug.Log("furula");
		story.ChooseChoiceIndex (choice.index);
		RefreshView();
	}

	// Creates a textbox showing the the line of text
	void CreateContentView (string text) {
		Text storyText = Instantiate (textPrefab) as Text;
		storyText.text = text;
		storyText.transform.SetParent (canvas.transform, false);
	}

	// Creates a button showing the choice text
	Button CreateChoiceView (string text) {
		// Creates the button from a prefab
		Button choice = Instantiate (buttonPrefab) as Button;
		choice.transform.SetParent (canvaschoices.transform, false);
		
		// Gets the text from the button prefab
		Text choiceText = choice.GetComponentInChildren<Text> ();
		choiceText.text = text;

		// Make the button expand to fit the text
		HorizontalLayoutGroup layoutGroup = choice.GetComponent <HorizontalLayoutGroup> ();
		layoutGroup.childForceExpandHeight = false;

		return choice;
	}

	// Destroys all the children of this gameobject (all the UI)
	void RemoveChildren () {
		int childCount = canvas.transform.childCount;
		for (int i = childCount - 1; i >= 0; --i) {
			GameObject.Destroy (canvas.transform.GetChild (i).gameObject);
		}

        childCount = canvaschoices.transform.childCount;
        for (int i = childCount - 1; i >= 0; --i)
        {
            GameObject.Destroy(canvaschoices.transform.GetChild(i).gameObject);
        }

        //GameObject[] botones = GameObject.FindGameObjectsWithTag("boton");
        //for (int i = botones.Length - 1; i >= 0; --i)
        //{
        //    GameObject.Destroy(botones[i]);
        //}
    }


    [SerializeField]
	private TextAsset inkJSONAsset = null;
	public Story story;

	[SerializeField]
	public GameObject canvas = null;
    public GameObject canvaschoices = null;

    // UI Prefabs
    [SerializeField]
	private Text textPrefab = null;
	[SerializeField]
	private Button buttonPrefab = null;
}
