INCLUDE Haroldo


-> Intro

==Intro==

#audio: Blackpink
#BG : Boutique
... # speaker: Zaida #portrait: Zaida_Neutral
...
...
Hala, ya está. Ahora que hice lo que madre me dijo puedo volver a dibujar. Que organice la dependienta luego.  
Pasen, tengo toda la colección ahí mismo. Les aseguro que las prendas merecen todos los eventos que he mencionado antes. # speaker: Khadija #portrait: Khadija 
Mierda… ¡era hoy la reunión con los inversores! # speaker: Zaida #portrait: Zaida_Asustada
Serán la nueva norma de la moda este invierno. Cuando la gente lo vea nadie podrá… # speaker: Khadija #portrait: Khadija
¡Zaida! ¿Qué has hecho? ¿Por qué está todo por el suelo? #portrait: Khadija_Enfado
Buenas tardes, señores, madre. Solo estaba haciendo la campaña de marketing de las redes sociales. # speaker: Zaida #portrait: Zaida_Neutral
¡Te dije que la hicieras hace dos semanas! Y encima has dejado toda la ropa por el suelo. (suspira) Luego hablaremos tu y yo, señorita, sube a tu cuarto. # speaker: Khadija #portrait: Khadija_Enfado
"Puff." # speaker: Zaida #portrait: Zaida_Enfado
"¡Tampoco es para tanto! Como si hubiese roto algo, solo estaba un poco desordenado. No entiendo por qué tiene que ponerse así." # speaker: Zaida
Lamento muchísimo que hayan tenido que ver esta escena, no sé qué le pasa últimamente a mi hija, les prometo que no volverá a pasar. # speaker: Khadija #portrait: Khadija
"¡A mí no me pasa nada!  Además, qué les importará a ellos, solo tienen que darle el visto bueno a la ropa y apoquinar dinero." # speaker: Zaida #portrait: Zaida_Enfado
"A ver qué dicen…"  #portrait: Zaida_Neutral
No se preocupe, estará con la regla y ya sabemos de sobra cómo os ponéis las mujeres, ¿verdad, señores? (risas) # speaker: Inversor 1
"¡¿Pero qué dice?! ¿Y la maleducada soy yo? Ese imbécil no tiene ni una pizca de respeto, pena me da la pobre pareja que tenga que aguantarlo todos los días." # speaker: Zaida #portrait: Zaida_Enfdado
"Solo por ese comentario intentaría sacarle un par de cientos de miles más. Venga, madre, destrozalo."
Si, si, es cierto, lo siento muchísimo. Que vergüenza me da cuando no es capaz de mantener los modales… (se agacha a recoger la ropa). # speaker: Khadija #portrait: Khadija
 "¿Qué…? Pero… ¿Qué haces? ¿Por qué no le dices nada?" # speaker: Zaida #portrait: Zaida_Enfado
Lo entendemos, aunque por esto mismo me cuesta confiar en vosotras, un error así por cambios hormonales y todo un proyecto se va al traste. # speaker:Inversor 2 
Pero cómo es usted, alguien que nos ha demostrado que puede ser confiable en cualquier momento, no se lo tendremos en cuenta. 
Muchísimas gracias. # speaker: Khadija #portrait: Khadija
"¿Por qué te agachas ante esa panda de gilipollas? Tú sí que me avergüenzas…" # speaker: Zaida #portrait: Zaida_Enfado

->Cuarto

==Cuarto==

#audio: Neutral
#BG : CuartoZaida
¿Cómo te atreves a dejarme así de mal ante los inversores? Menos mal que he podido salvar la situación si no no habría colección de invierno ni de otoño ni nunca más. # speaker: Khadija #portrait: Khadija_Enfado
Estás exagerando un poco. Aunque esos tíos no te hubiesen dado el dinero nuestra marca es muy conocida, cualquiera se pegaría por patrocinarte.  Y luego se arrepentirían toda su vida. # speaker: Zaida #portrait: Zaida_Neutral
No tienes ni idea… Crees que las cosas se consiguen por arte de magia y que cuando eres grande no puedes caer pero el mundo no funciona así, y menos para nosotras. # speaker: Khadija #portrait: Khadija_Enfado
Un error así, una mala reputación, te puede cerrar muchas puertas, incluso las que ya estaban abiertas. 
Después de todo lo que he hecho por tí… Levanté desde cero esta empresa de moda con el dinero de la herencia solo por darte un futuro mejor después de que tu padre nos abandonara. 
Y tú me lo agradeces así: ¡comportándose como una cria!, ¡tardando 2 semanas en hacer una simple foto para instagram y desorganizando todo el día más importante del proceso de la producción! 
¿A mi me tratas así por esta tontería pero luego te agachas ante esos tipos después de los comentarios que han dicho? # speaker: Zaida #portrait: Zaida_Enfado
¿Cómo puedes dejar que nos pisoteen de esa manera cuando son ellos los que van a ganar dinero con tu trabajo? ¡Deberías haberlos echado a patadas!
¡Ni se te ocurra volver a hablarme así, señorita! # speaker: Khadija #portrait: Khadija_Enfado
Las cosas no funcionan así. 
Tenemos que agacharnos para luego poder alzarnos, así es como es el mundo y ya va siendo hora de que lo aprendas.
Estoy harta de estas rabietas infantiles, Zaida. Lo hablé con mi abogado y ya eché los papeles. Este curso que viene, irás al Internado San Muröi.

#portrait: Todos
#BG: Transición
#audio: Null



->PH
->DONE