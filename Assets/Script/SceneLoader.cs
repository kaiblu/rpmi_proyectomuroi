using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class SceneLoader : MonoBehaviour
{

    public Animator fade;
    public VideoPlayer Opening;

    public void Fade()
    {

        fade.enabled = true;



    }


    public void CargarEscenaA1()
    {


        SceneManager.LoadScene("EscenaA1");

    }

    public void CargarEscena2()
    {


        SceneManager.LoadScene("Escena2");

    }

    public void CargarEscena3()
    {


        SceneManager.LoadScene("Escena3");

    }

    public void CargarEscena4()
    {


        SceneManager.LoadScene("Escena4");

    }

    public void CargarMenú()
    {


        SceneManager.LoadScene("Menu");

    }
    public void CargarEpilogo()
    {


        SceneManager.LoadScene("Epilogo");

    }


    public void Salir()
    {

        Application.Quit();


    }

   /* public void NoaniZ()
    {
        if (!Opening.isPlaying)
        {
            Debug.Log("escena");
            SceneManager.LoadScene("Escena2");


        }

    }*/
   


}
