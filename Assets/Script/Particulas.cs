using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particulas : MonoBehaviour
{

    public Camera particula;
    public ParticleSystem jaj;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {


        if (Input.GetMouseButtonDown(0))
        {
            var emission = jaj.emission;
            emission.enabled = true; 
            Vector3 position = particula.ScreenToWorldPoint(Input.mousePosition);
            position.z = 0;
            transform.position = position;

        }

        else if (Input.GetMouseButtonUp(0))
        {

            var emission = jaj.emission;
            emission.enabled = false;

        }





    }
}
