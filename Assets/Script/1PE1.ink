INCLUDE 1PE2


->Intro

==Intro==

#audio: Neutral
#portrait: Todos
#BG: Transición
#BG: Recepcion

… #speaker: Izai #portrait: Izai_Neutral
… #speaker: Haro #portrait: Haro_Neutral
… #speaker: Walid #portrait: Walid_Neutral
Hola… #speaker: Zaida #portrait: Zaida_Neutral
Hola #speaker: Todos #portrait: Todos
… 
¿Es que aquí no hay nadie? Este sitio es peor de lo que creía. #speaker: Zaida #portrait: Zaida_Enfado
Concuerdo contigo, morena. Este sitio apesta, no hay criados y está situado a 15 kilómetros del pueblo más cercano. #speaker: Haro #portrait: Haro_Guino
Ya me di cuenta al venir. Pero lo peor es que no puedo llevar la ropa de mi madre, ¿sabes que hay uniformes? #speaker: Zaida #portrait: Zaida_Molesta
Si, pero como soy un chico eso no me importa #speaker: Haro #portrait: Haro_Feliz
¡¿Disculpa?! #speaker: Zaida #portrait: Zaida_Enfado
¿Sabéis lo peor? El wifi solo se activa de 8 a 10 de la tarde y los dispositivos electrónicos están limitados a los del centro. #speaker: Walid #portrait: Walid_Triste
Seguro que con los ordenadores basura que nos darán no podré hacer nada.
¿Solo 2 horas? Bueno, es soportable. #speaker: Zaida #portrait: Zaida_Feliz
Cómo tampoco soy un friki marginado eso no me importa. #speaker: Haro #portrait: Haro_Feliz
(risa contenida) #speaker: Walid #portrait: Walid_Feliz
¿Qué te hace tanta gracia?  #speaker: Haro #portrait: Haro_Enfado
Nada, nada. #speaker: Walid #portrait: Walid_Feliz
… #speaker: Izai #portrait: Izai_Neutral
¿En serio aún no viene nadie? Pero si nos han abierto, ¡saben que estamos aquí!  #speaker: Zaida #portrait: Zaida_Enfado
Morena, hazte a la idea de que este sitio es un asco y no tienen modales ni respeto.  #speaker: Haro #portrait: Haro_Feliz
Por desgracia coincido con el bufón. #speaker: Walid #portrait: Walid_Neutral
¿Cómo me has llamado? #speaker: Haro #portrait: Haro_Enfado
¡Quiero volver a casa! Aquí no nos van a dar nada de tiempo libre… #speaker: Zaida #portrait: Zaida_Triste
Yo también. Aquí no podré ni tocar el ordenador. #speaker: Walid #portrait: Walid_Triste
No os quejéis lo mio es peor. Aquí nunca podré ir a Italia a comer pizza con mi padre. #speaker: Haro #portrait: HAro_Triste

#speaker: Izai #portrait: Izai_Enfado
* Quejicas...  #speaker: Izai #portrait: Izai_Enfado
  Esta es la escuela más prodigiosa del continente y vosotros os amargais por nimiedades. #portrait: Izai_Neutral
  ->risas

* ¿Sabéis lo que es peor[, niñatos] ? #portrait: Izai_Enfado
  Tener que irte a la otra punta del país a estudiar en un colegio con personas que no son conscientes del privilegio que tienen por estar aquí y no hacen más que quejarse por tonterías. #portrait: Izai_Neutral
  ->risas
  
  =risas
  
(se aguanta la risa) #speaker: Zaida #portrait: Zaida_Feliz
(sonríe con maldad) #speaker: Walid #portrait: Walid_Feliz
JAJAJAJAJA #speaker: Haro #portrait: Haro_Feliz
¿Qué? ¿Acaso dije algo gracioso? #speaker: Izai #portrait: Izai_Enfado
JJAJAJAJAJ #speaker: Zaida #portrait: Zaida_Feliz
(risa contenida) #speaker: Walid #portrait: Walid_Feliz
Pobretona… #speaker: Haro #portrait: Haro_Feliz

*Malditos ricos  #speaker: Izai #portrait: Izai_Enfado
->presentacion
*[Te quedas callada] ... #speaker: Izai #portrait: Izai_Enfado
->presentacion

=presentacion

#portrait: Todos

Señor Haroldo Rajoy, #speaker: ??? 
señor Walid Minami, 
señorita Zaida Habib, 
y señorita Izai Gónzalez; 
Soy el director Andrés, es un honor contar con vosotros este curso. Espero que su estancia en esta inpecable institución sea de su agrado y puedan considerarla su segundo hogar.  #speaker: Andrés #portrait: Andrés_Feliz
Acompáñenme, les enseñaré el edificio y les daré sus horarios y habitaciones. 
Bienvenidos al Internado San Muröi.

#BG: Transición
#audio: Null

->P1