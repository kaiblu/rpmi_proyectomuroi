-> Intro

==Intro==
#BG: ClaseMuroi1
#audio: Neutral

Esto es muy difícil… Cómo odio dibujo técnico. #speaker: Haro #portrait: Haro2_Enfado
No tanto. #speaker: Walid #portrait: Walid2_Neutral
¡Pero si no tiene ni pies ni cabeza! #speaker: Haro #portrait: Haro2_Enfado
No, tiene número y formas geométricas. #speaker: Walid #portrait: Walid2_Feliz
… #speaker: Haro #portrait: Haro2_Neutral
… #speaker: Walid #portrait: Walid2_Feliz
¿Quién es el bufón ahora? #speaker: Haro #portrait: Haro2_Feliz
¿Y quién es el friki que me pidió la play anoche, eh? #speaker: Walid #portrait: Walid2_Preocu
Touché. #speaker: Haro #portrait: Haro2_Feliz

->P1

==P1==
Me encantan las clases de declinar palabras, son tan relajantes como las de sintaxis… #speaker: Izai #portrait: Izai2_Feliz
En serio, creo que hay algo que no funciona bien en tu cabeza. #speaker: Zaida #portrait: Zaida2_Preocu
Si no te gustan las letras, ¿por qué no te matriculaste en arte? #speaker: Izai #portrait: Izai2_Neutral
Mucha escuela privada pero no tienen muchas opciones. #speaker: Zaida #portrait: Zaida2_Enfado

->P2

==P2==
#BG: Patio
#audio: Feliz

Hola, chicos. ¿Algo interesante? #speaker: Zaida #portrait: Zaida2_Feliz
No. #speaker: Walid #portrait: Walid2_Neutral
Para nada. #speaker: Haro #portrait: Haro2_Triste
Los jueves son horribles. #speaker: Zaida #portrait: Zaida2_Triste
Sobre todo cuando llega Izai con su dichosa revistita a recordarnos que estamos en peligro constante porque unos aliens abducen alumnos. #speaker: Haro #portrait: Haro2_Feliz
Poca broma con eso, estoy seguro de que si mi abuelo no me llega a pillar habría visto los informes secretos de extraterrestres de la NASA. #speaker: Walid #portrait: Walid2_Feliz
Sí… Claro.. Lo que tu digas amigo. #speaker: Haro #portrait: Haro2_Feliz
¡Hola, Izai! #speaker: Zaida #portrait: Zaida2_Feliz
¿Algo interesante en la revista hoy? #speaker: Walid #portrait: Walid2_Neutral

*Que raro.. #speaker: Izai #portrait: Izai2_Neutral
¿Qué pasa? #speaker: Zaida #portrait: Zaida2_Preocu
¿Hoy han ‘desaparecido’ tres o que? #speaker: Haro #portrait: Haro2_Feliz
Naide… #speaker: Izai #portrait: Izai2_Preocu
¿Cómo? #speaker: Walid #portrait: Walid2_Neutral  
->P2a

*Pues la verdad es que si.  #speaker: Izai #portrait: Izai2_Preocu
¿El qué? #speaker: Zaida #portrait: Zaida2_Preocu
->P2a

=P2a
Esta semana no ha desaparecido nadie… #speaker: Izai #portrait: Izai2_Neutral
… #speaker: Haro #portrait: Haro2_Sorpresa
… #speaker: Zaida #portrait: Zaida2_Sorpresa
Nos hemos acostumbrado a las desapariciones tanto que ahora nos resulta extraño que no hayan… #speaker: Walid #portrait: Walid2_Preocu
¿Pero por qué de repente han parado? #speaker: Zaida #portrait: Zaida2_Preocu
¿La semana pasada desaparecieron dos chicas y ahora nadie?
No tengo ni idea, pero… ¿no tenéis curiosidad? #speaker: Izai #portrait: Izai2_Preocu
Esto si que es extraño… #speaker: Walid #portrait: Walid2_Neutral
Pues sí… #speaker: Zaida #portrait: Zaida2_Triste
No mucha. #speaker: Haro #portrait: Haro2_Feliz
El otro día fuiste tú el que se dio cuenta de que Karen no estaba. #speaker: Zaida #portrait: Zaida2_Enfado
Y el que mencionó el tema en el cuarto. #speaker: Walid #portrait: Walid2_Feliz
¿No tienes ni una pizca de curiosidad sobre lo que puede estar pasando? #speaker: Izai #portrait: Izai2_Neutral
¡¿Y qué?! #speaker: Haro #portrait: Haro2_Enfado
Mientras no me pase a mí… #portrait: Haro2_Feliz
Miedica. #speaker: Zaida #portrait: Zaida2_Feliz
Miedica no, sensato. #speaker: Haro #portrait: Haro2_Neutral
Imagínate que de verdad está pasando algo grave, ¿de verdad queréis poneros en peligro? 
¡Estais locos! #portrait: Haro2_Enfado
No es de locos informarse. #speaker: Walid #portrait: Walid2_Triste
Que sí, que sí, perdona friki paranoico. #speaker: Haro #portrait: Haro2_Neutral
De locos es meteros en estos líos sólo porque sí. ¿Que la gente se pira del internado? ¡Allá ellos! #portrait: Haro2_Enfado

*No se piran, desaparecen. #speaker: Izai #portrait: Izai2_Enfado
->P2b
*¿Y si desapareces tú? #speaker: Izai #portrait: Izai2_Enfado
->P2b

=P2b
#audio: Acción
¡Y dale con eso! #speaker: Haro #portrait: Haro2_Enfado
A mi también me gustaría saber qué pasa. #speaker: Zaida #portrait: Zaida2_Neutral
¿En serio, Zaida? #speaker: Haro #portrait: Haro2_Preocu
Si algo peligroso está ocurriendo cerca nuestra, cuanto antes lo sepamos mejor. #speaker: Walid #portrait: Walid2_Neutral
Vale, muy bien. Pero, ¿y si nos pillan fisgoneando? ¡Nos expulsarían! #speaker: Haro #portrait: Haro2_Preocu
Walid,  eres el nieto del ministro de la embajada japonesa. ¿Qué te diría tu abuelo si ve que no has madurado y sigues metiéndote en líos por desconfiar de la gente? ¿Qué crees que haría contigo entonces? 
Pensará que eres un caso perdido, que nunca superarás lo de tus padres y no confiará en tí en el futuro.
… #speaker: Walid #portrait: Walid2_Triste
Zaida, eras la hija de la dueña de la compañía de moda de alta clase más conocida del mundo. ¿Qué pasaría si todos los inversores supieran que la heredera de la empresa es una inconsciente a la que echaron del San Muröi? #speaker: Haro #portrait: Haro2_Preocu
Nadie confiaría en tí para llevar la empresa y el negocio que tanto le costó a tu madre montar se iría a la mierda. Por no hablar de lo decepcionada que se sentiría.
Si yo ni siquiera… #speaker: Zaida #portrait: Zaida2_Enfado
Ya sé que no quieres llevar el negocio, pero en algún momento tú también serás responsable, de la forma que sea. #speaker: Haro #portrait: Haro2_Preocu
… #speaker: Zaida #portrait: Zaida2_Triste
Yo soy el hijo del mismísimo ex presidente de España, Mariano Rajoy. #speaker: Haro #portrait: Haro2_Preocu
Si me expulsan la opinión pública volverá a dirigir la mirada hacia mi padre y no le dejarán tranquilo. Entre los casos de corrupción abiertos y que no ha podido mantener a raya a su hijo, su credibilidad y su apoyo caerían y quién sabe si acaba en la cárcel…
No podría volver a comer pizza en Italia con él. #portrait: Haro2_Triste


*[¿Eso es lo único que te preocupa?] #speaker: Izai #portrait: Izai2_Enfado
¿Eso es lo único que te preocupa entonces?
¿Perder vuestro privilegio?
Yo he vivido toda mi vida siendo una persona no binaria, bisexual, pobre y gorda. No tengo ninguno de esos privilegios que tú temes perder. 
No tengo unos padres ricos que me hayan solucionado la vida antes de nacer. No tengo el poder de estafar a todo el pueblo como tu padre. Ni tengo el conocimiento de saber todo lo que nos oculta gente como vosotros.
Yo tengo que luchar por mi misma para tener una décima aparte de lo que tú tienes por respirar. 
->P2c
*¿Prefieres desaparecer entonces? #speaker: Izai #portrait: Izai2_Preocu
->P2c

=P2c
Izai, ¿y tú que? #speaker: Haro #portrait: Haro2_Neutral

*... #speaker: Izai #portrait: Izai2_Neutral
->P2d
*No tengo nada que perder. #speaker: Izai #portrait: Izai2_Enfado
En realidad creo que eres la que más tiene que perder. #speaker: Haro #portrait: Haro2_Neutral
->P2d

=P2d
Tú vienes de una familia pobre. Lo que para nosotros es una cárcel para tí es la oportunidad de tu vida. #speaker: Haro #portrait: Haro2_Neutral
El prestigio que te da esta escuela te abrirá cualquier puerta en el futuro. ¿de verdad quieres que tu expediente diga que te expulsaron de aquí? 
Tanto esfuerzo que te costó entrar aquí para nada, para decepcionar a todo el mundo y volver a tu pueblo con las manos vacías. 
… #speaker: Izai #portrait: Izai2_Triste
“Mis padres… mis compañeros y amigos…  mis profesores…”
“Ya había olvidado este sentimiento.”
“Ya no recordaba lo que era sentir que no soy suficiente para todos, tener miedo de ver cómo las expectativas de los demás sobre mi  se rompen otra vez…”
“Estos meses aquí me han ayudado mucho para darme cuenta que incluso entre esta gente que está por encima de mi, valgo mucho y destaco en mi ámbito.”
“No tengo nada que envidiarles. Somos iguales.” #portrait: Izai2_Feliz
“Al igual que no tengo nada que demostrar a nadie. Yo soy como soy, y así está bien.”
¿Eso es lo que quieres? ¿Perder la beca y decepcionar a todo el mundo? #speaker: Haro #portrait: Haro2_Preocu

*[Eso no va a pasar.] #speaker: Izai #portrait: Izai2_Enfado
Primero, no tienen por qué pillarnos.
Segundo, no estaremos haciendo nada malo. ¡No pueden expulsarnos porque sí!
->P2e
*[Eso no importa.] #speaker: Izai #portrait: Izai2_Enfado
Eso no importa ahora, prefiero investigar y sentirme a salvo que vivir con miedo.
->P2e

=P2e
¿Y si mejor lo dejamos estar? #speaker: Walid #portrait: Walid2_Triste
No tenemos por qué saberlo todo. #portrait: Walid2_Neutral
¿Qué? #speaker: Izai #portrait: Izai2_Sorpresa
Hay secretos que es mejor que sigan siendo secretos. #speaker: Walid #portrait: Walid2_Neutral
Al menos eso es lo que dice mi abuelo.
Seguro que es cosa de un demonio o una maldición… #speaker: Zaida #portrait: Zaida2_Feliz
¿Tú también Zaida? #speaker: Izai #portrait: Izai2_Sorpresa
Lo siento, Izai. #speaker: Zaida #portrait: Zaida2_Triste
¿Y si el siguiente fuera uno de nosotros? ¿Preferís vivir con un leve castigo o desaparecer como los demás? #speaker: Izai #portrait: Izai2_Enfado
Mejor vivir, sin más. Sin complicaciones ni problemas. #speaker: Haro #portrait: Haro2_Feliz
Lo que pasa es que tienes miedo. #speaker: Izai #portrait: Izai2_Enfado
Lo que pasa es que como tú no eres útil en la sociedad no te importa si te pillan. #speaker: Haro #portrait: Haro2_Enfado
… #speaker: Izai #portrait: Izai2_Triste
¡Haro! #speaker: Zaida #portrait: Zaida2_Sorpresa
Te has pasado de la ralla. #speaker: Walid #portrait: Walid2_Neutral

*[Tiene razón.] #speaker: Izai #portrait: Izai2_Neutral
No, dejadlo.
Si tiene razón.
->P2f
*Ojalá desaparezcas. #speaker: Izai #portrait: Izai2_Enfado
->P2f

=P2f
¡Izai! ¡Espera! #speaker: Zaida #portrait: Zaida2_Sorpresa
Déjala sola un rato. #speaker: Walid #portrait: Walid2_Preocu
Te has lucido, Haro. #speaker: Zaida #portrait: Zaida2_Enfado
… #speaker: Haro #portrait: Haro2_Triste
Puede que sí. 
¡Corre y pídele perdón ahora mismo! #speaker: Zaida #portrait: Zaida2_Enfado
… #speaker: Haro #portrait: Haro2_Triste
Es demasiado orgullo, pero acabará haciéndolo. #speaker: Walid #portrait: Walid2_Neutral
¿Verdad? (le pasa el brazo por el hombro) #portrait: Walid2_Feliz
Odio que me conozcas tanto. #speaker: Haro #portrait: Haro2_Feliz

->P3

==P3==
#BG: Transición
#audio: Null
... #speaker: Izai #portrait: Izai2_Triste
*[Ir al baño] #speaker: Izai #portrait: Izai2_Neutral
->Baño
*[Ir con Akil] #speaker: Izai #portrait: Izai2_Neutral
->Akil

==Baño==
#BG: Baño
#audio: Triste
Malditos pijos… #speaker: Izai #portrait: Izai2_Enfado
¿Cómo pude llegar a pensar que podíamos ser amigos?
Solo son una panda de malcriados que han tenido todo lo que han querido siempre. 
¡Estoy harta! No les soporto más. Quiero irme a mi casa…
... #portrait: Izai2_Triste
“No, Izai, tienes que ser fuerte. Tienes que aguantar un poco más”
“Ya mismo terminará el curso y podrás descansar de esta pesadilla de gente. Tienes que aguantar y sobrevivir. Y si para ello tengo que investigar yo sola lo que está pasando aquí, lo haré.” 
->P4

==Akil==
#BG: Biblioteca
#audio: Acción
¡¡Malditos ricos!!! Solo piensan con el puto dinero (da un golpe en la mesa)  #speaker: Izai #portrait: Izai2_Enfado
¡Si no tienes dinero y poder no importas en la sociedad, y si no le importas a la sociedad no eres nada! ¡Odio este sistema! 
Te doy toda la razón. Los ricos son egoístas, avariciosos, estúpidos y creídos; no valoran lo que tienen y son demasiado poderosos. #speaker: Akil #portrait: Akil_Neutral
Pero, ¿qué ha pasado? #portrait: Akil_Preocu
Haroldo es imbécil. #speaker: Izai #portrait: Izai2_Enfado
¿Qué esperas del hijo de M. Rajoy? #speaker: Akil #portrait: Akil_Feliz
(risa tímida) Si… #speaker: Izai #portrait: Izai2_Triste
Ahora en serio, no le hagas caso. Sea lo que sea que te haya dicho seguro que es mentira. #speaker: Akil #portrait: Akil_Feliz
Ya sabes cómo suele ser la gente aquí: Cada uno vive en su propia burbuja dorada y no son capaces de mirar más allá.
Pero tampoco podemos culparlos por ello. Simplemente no saben lo que es vivir sin nada. Desde su perspectiva todo esto que nos parecen lujos, es normal.
¿Por qué? Por qué permitimos esta injusticia? Si al menos todos fuéramos iguales, si al menos se portaran bien con todos…  #speaker: Izai #portrait: Izai2_Triste
Somos iguales. ¿Es que cuando te has peleado con Haroldo no habéis estado al mismo nivel? Incluso diría que conociéndote te has puesto tú por encima de él. #speaker: Akil #portrait: Akil_Feliz
Ubi sunt.

*El tiempo se va. #speaker: Izai #portrait: Izai2_Neutral
Eso es el tempus fugit. ¿Y tú eres una becada? ¿Cómo es posible que no conozcas este tópico literario? (se ríe) #speaker: Akil #portrait: Akil_Feliz
->Akila

*¿Dónde están? #speaker: Izai #portrait: Izai2_Feliz
Exacto. #speaker: Akil #portrait: Akil_Feliz
->Akila

=Akila
El ubi sunt hace referencia a la pérdida de los bienes del mundo material. Cuando pasas al mundo de las ideas, todo lo terrenal permanece hasta convertirse en polvo, pero tu persona sigue viviendo en la eternidad de lo étereo. #speaker: Akil #portrait: Akil_Feliz
Platón. #speaker: Izai #portrait: Izai2_Neutral
Así es. #speaker: Akil #portrait: Akil_Neutral
Y en la literatura lo utilizaban para dar a entender a los poderosos y a los ricos, que no importa cuánto posean ahora, porque luego van a perderlo todo al final. 
Y es verdad, la vida es muy efímera, nunca sabemos cuándo puede ser el último día. Y al final lo importante no será cuánto dinero hayas tenido, sino cómo te recuerdan los demás.

*[No hay mundo de las ideas.] #speaker: Izai #portrait: Izai2_Preocu
Pero yo no creo que exista esa vida después de la muerte. No tiene sentido…
Yo tampoco lo creo. #speaker: Akil #portrait: Akil_Feliz
Pero sí que es cierto el tópico. Al final, por mucho que tengas, puede pasar cualquier cosa que te haga perderlo todo, y no tiene que ser necesariamente la muerte.
Revoluciones, malas rachas, enfermedades, pandemias, crisis, pérdidas, reputaciones,...
Hoy en día cualquier cosa puede acabar con tu estabilidad. Sino mira a los NFTeros.
(Se ríe) Eso es verdad. #speaker: Izai #portrait: Izai2_Feliz
->Akilb

*Pero se vive mejor con dinero. #speaker: Izai #portrait: Izai2_Feliz
Hombre, eso por supuesto (se ríe) #speaker: Akil #portrait: Akil_Feliz
Yo sería más feliz así también.
->Akilb

=Akilb
Pero entiendo lo que dices. #speaker: Izai #portrait: Izai2_Neutral
Gracias, Akil. #portrait: Izai2_Feliz
No me las des. #speaker: Akil #portrait: Akil_Feliz

PIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII #speaker: Timbre #efecto: timbre

Venga, ahora a clase. #speaker: Akil #portrait: Akil_Neutral
¿Qué tienes ahora? 
TIC, y tu? #speaker: Izai #portrait: Izai2_Neutral
Filosofía #speaker: Akil #portrait: Akil_Triste
¡Hasta luego! #portrait: Akil_Feliz
->P4

==P4==
#BG: ClaseMuroi1
#audio: Neutral2
¿Ya has terminado la práctica de las tablas del excel?  #speaker: Izai #portrait: Izai2_Neutral
¿Me tomas el pelo? Eso se hace en cinco minutos. (se ríe) #speaker: Walid #portrait: Walid2_Feliz
¿Qué haces? #portrait: Walid2_Neutral
Busco información sobre los desaparecidos.  #speaker: Izai #portrait: Izai2_Neutral
Manuel Hernandez, Karen y Patricia, Mohamed Alabi y Chris Pérez. Todos llegaron al internado por motivos distintos. Manuel y Mohamed por sus notas, como yo, los demás como vosotros por pertenecer a la ‘clase alta’. 
No lo digas así… #speaker: Walid #portrait: Walid2_Triste

*Es la verdad. #speaker: Izai #portrait: Izai2_Neutral
->P4a

=P4a
Pero lo dices cómo si eso nos diferenciara y no es así. #speaker: Walid #portrait: Walid2_Neutral
Ambos somos personas, y somos iguales. Ambos llegamos por esforzarnos en conseguir nuestros objetivos, aunque el mío fuera ilegal. 
Lo sé, lo sé (se ríe) #speaker: Izai #portrait: Izai2_Feliz
En fin, sigue contándome. #speaker: Walid #portrait: Walid2_Neutral
¡Ah, si! #speaker: Izai #portrait: Izai2_Sorpresa
… #portrait: Izai2_Neutral
No estaban en las mismas habitaciones… 
Solo Patricia y Mohamed compartían la misma clase. No tienen nada en común a parte del supuesto club íntimo que nos contó el director.
Aparentemente… #speaker: Walid #portrait: Walid2_Preocu
(La mira) #portrait: Walid2_Feliz
(Le mira) #speaker: Izai #portrait: Izai2_Neutral

*[Hackear a la escuela.] #speaker: Izai #portrait: Izai2_Neutral
Cómo me gusta tu forma de pensar, paranoico. #portrait: Izai2_Feliz
(sonríe) #speaker: Walid #portrait: Walid2_Feliz
->P4b

*[No hackear a la escuela] #speaker: Izai #portrait: Izai2_Neutral
¿No decías que era muy peligroso y que no querías volver a hacer nada ilegal? ¿Qué pasa con tu abuelo?
Solo una última vez, nadie se enterará, no pasará nada. #speaker: Walid #portrait: Walid2_Neutral
¿Lo dices para convencerme a mí o para convencerte a tí? #speaker: Izai #portrait: Izai2_Feliz
¿Ambas? #speaker: Walid #portrait: Walid2_Neutral
¿A quién voy a engañar? No puedo salir de la droga.
->P4b

=P4b
… #speaker: Walid #portrait: Walid2_Neutral
…
…
Date prisa, yo aviso si se acerca el profesor. #speaker: Izai #portrait: Izai2_Preocu
Vale, no tengo mis programas así que tardaré un poco más en pasar el cortafuegos. Al menos el ordenador está conectado a la red. #speaker: Walid #portrait: Walid2_Neutral
…
…
Ya estoy dentro.
Abre todos sus expedientes. #speaker: Izai #portrait: Izai2_Neutral
 Listo. #speaker: Walid #portrait: Walid2_Feliz
Ahora yo vigilo.  #portrait: Walid2_Neutral
#portrait: Todos

#Interfaz: Manuel
#Interfaz: Patricia
#Interfaz: Karen
#Interfaz: Chris

.
.
.
.

Ni las notas, ni las clases, ni las aulas… #speaker: Izai #portrait: Izai2_Triste
Joder hasta parece a propósito.
¿Y si lo fuera? #speaker: Walid #portrait: Walid2_Neutral
“Parece que no hay nada interesante…” #speaker: Izai #portrait: Izai2_Triste
“Pensaba que podría encontrar algo clave para resolver este misterio pero parece que no tenían nada en común…”
“Bueno, espera… Sí que había algo”  #portrait: Izai2_Neutral


*[Piedad] #speaker: Izai
“Es verdad, a todos les daba clase de  Filosofía la profesora Piedad… Aunque tampoco creo que sea algo muy relevante.”
“Puede ser una coincidencia o no.”
“Por si acaso mejor lo tengo en cuenta y tengo más cuidado con ella.”
->P4c

*[Andrés] #speaker: Izai
“¡Es verdad! Todos habían tenido tutoría con el director una semana antes de desaparecer.”
“Esto si que es raro… Aunque puede ser que simplemente Andrés nos contara la verdad y estuviese investigando ese supuesto club de apuestas.”
->P4c

=P4c
¿Qué pasa? #speaker: Walid #portrait: Walid2_Neutral
No, nada. #speaker: Izai #portrait: Izai2_Feliz
Desconéctate de esto. #portrait: Izai2_Neutral
#portrait: Todos

PIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII #speaker: Timbre #efecto: timbre

->P5

==P5==
#BG: ClaseMuroi1
#audio: Acción
Hoy vamos a ver las distintas discriminaciones que hace nuestra sociedad, por ejemplo, unos de los más hablados, el racismo y la xenofobia, que es… #speaker: Doña Piedad #portrait: Piedad_Neutral
Izai #speaker: Haro #portrait: Haro2_Neutral
(le tira una bola de papel)
Izai
(le tira una bola de papel)
Me ignora. #portrait: Haro2_Enfado
Normal #speaker: Walid #portrait: Walid2_Neutral
Eso, tú anímame más. #speaker: Haro #portrait: Haro2_Triste
Izai  #portrait: Haro2_Neutral
(le tira un papel)
¡Izai!

*[Tirarle una.] #speaker: Izai #portrait: Izai2_Neutral
¡Oye! #speaker: Haro #portrait: Haro2_Enfado
(risa contenida) #speaker: Zaida #portrait: Zaida2_Feliz
->P5a
*[Ignorarle.] #speaker: Izai #portrait: Izai2_Neutral
->P5a

=P5a
Zaida #speaker: Walid #portrait: Walid2_Neutral
Dime #speaker: Zaida #portrait: Zaida2_Neutral
Llama a Izai, anda. #speaker: Walid #portrait: Walid2_Neutral
Walid te llama, Izai. #speaker: Zaida #portrait: Zaida2_Neutral

*[Lo sé.] #speaker: Izai #portrait: Izai2_Neutral
Soy consciente de ello.
->P5b

*¿Qué quiere? #speaker: Izai #portrait: Izai2_Neutral
->P5b

=P5b
Haro quiere pedirte perdón. #speaker: Zaida #portrait: Zaida2_Neutral
Está arrepentido. ¿Por qué no lo aprovechas y le obligas a hacer algo? 
No sé, que sea tu esclavo una semana o algo así. #portrait: Zaida2_Feliz
No, gracias. #speaker: Izai #portrait: Izai2_Neutral
Nada. #speaker: Zaida  #portrait: Zaida2_Preocu
Joder… ¡así cómo voy a pedirle perdón! #speaker: Haro #portrait: Haro2_Enfado
Ten paciencia, las cosas no se arreglan de un día para otro. #speaker: Walid  #portrait: Walid2_Neutral
¡Paciencia! ¡Encima que la loca es ella! #speaker: Haro #portrait: Haro2_Enfado

*[Ignorarle] #speaker: Izai #portrait: Izai2_Neutral
…
->P5c
*[Contestarle] #speaker: Izai #portrait: Izai2_Neutral
¡Disculpa, pero el que se pasó de la raya fuiste tú! #portrait: Izai2_Enfado
->P5c

=P5c
¡Ey! ¡Chicos! #speaker: Doña Piedad #portrait: Piedad_Enfado
¿Me estáis atendiendo?  #portrait: Piedad_Neutral
¡Si! #speaker: Todos #portrait: Todos
Más os vale, que esto cae en el examen. #speaker: Doña Piedad #portrait: Piedad_Neutral
“Izai…” #speaker: Zaida #portrait: Zaida2_Triste
->P6

==P6==
#BG: CuartoMuroi2
#audio: Noche

Izai, ¿podemos hablar? #speaker: Zaida #portrait: Zaida3_Preocu

*¿De qué? #speaker: Izai #portrait: Izai3_Neutral
->P6a
*Sí, dime. #speaker: Izai #portrait: Izai3_Neutral
->P6a

=P6a
Haroldo, ya le conoces…, a veces es un capullo, pero quiere arreglar las cosas. #speaker: Zaida #portrait: Zaida3_Preocu
Por muy buena intención que tenga lo que me dijo me ha dolido mucho y no pienso perdonarlo de un día para otro. #speaker: Izai #portrait: Izai3_Preocu
Y te entiendo, pero… estoy harta de esta tensión que se ha creado entre los cuatro desde que empezaron las desapariciones. #speaker: Zaida #portrait: Zaida3_Triste
Antes todo era diferente. No nos peleábamos tanto, ni siquiera vosotros que sois tan diferentes…
No sé, echo de menos estar bien con vosotros porque sois los primeros amigos de verdad que tengo. Y no quiero perder eso.


*... #speaker: Izai #portrait: Izai3_Triste
->P6b
*[Yo también] #speaker: Izai #portrait: Izai3_Triste
Yo también echo de menos la paz que teníamos. Pero es que con las desapariciones no puedo dejar de pensar que cualquiera de los cuatro puede ser el siguiente.
->P6b

=P6b
Deberíamos hacer algo. #portrait: Izai3_Preocu
¿En serio? ¿Me cambias de tema? #speaker: Zaida #portrait: Zaida3_Neutral
Es mejor dejar las cosas como están, no vayan a empeorar 
¿Tú no tienes miedo? Porque yo tengo miedo de perderos. #speaker: Izai #portrait: Izai3_Triste
¡No voy a desaparecer, Izai! #speaker: Zaida #portrait: Zaida3_Enfado
Déjalo ya. #portrait: Zaida3_Triste
Por favor.
¿No puedes simplemente ir a clases y vivir tu rutina de siempre sin preocuparte por los demás aunque sea una vez?
¡No puedes ayudar a todo el mundo! ¡¿Qué importa que no sepamos dónde están cuatro personas que ni siquiera conocemos?! #portrait: Zaida3_Enfado

*A mi me importa. #speaker: Izai #portrait: Izai3_Neutral
… #speaker: Zaida #portrait: Zaida3_Triste
… #speaker: Izai #portrait: Izai3_Triste
->P6c
*Tienes razón. #speaker: Izai #portrait: Izai3_Neutral
Vale, sí, tienes razón.
Debería dejar de preocuparme tanto…
Supongo que solo tengo miedo.
Es normal. Todos lo tenemos #speaker: Zaida #portrait: Zaida3_Triste
Pero no puede dejar que te domine.
Lo sé… #speaker: Izai #portrait: Izai3_Triste
...
->P6c

=P6c
Mejor me voy a dormir, ya es muy tarde. #speaker: Izai #portrait: Izai3_Neutral
Sí, supongo que sí. #speaker: Zaida #portrait: Zaida3_Preocu
Buenas noches. #portrait: Zaida3_Feliz
Buenas noches, Zaida. #speaker: Izai #portrait: Izai3_Neutral
->P7

==P7==
#BG: Transición
#audio: Null

“¿Qué hago?”  #speaker: Izai #portrait: Izai3_Preocu
“Chris es el único de los desaparecidos que no tuvo una tutoría con Andrés, lo cual confirma lo que dijo Akil de que le parecía muy rara la situación. Pero según el director él también estaba metido en ese club raro…” 
“¿Nos mintió? ¿Por qué? ¿Qué es lo que oculta? ¿Es el director el que está detrás de todo al final o está encubriendo algo?”
“Tenía pensado ir a echar un vistazo al cuarto de Chris mientras Akil está fuera dando su paseo nocturno, pero después de esta conversación con Zaida no sé si es buena idea…”
“A lo mejor estoy exagerando demasiado… No sé qué hacer…”

*[Dormir]
#Escena: BAD
->END
*[Ir a investigar]
->P8

==P8==
#BG: Pasillo2


Como me pillen por aquí me matan…  #speaker: Izai #portrait: Izai_Preocu
Mejor me doy prisa.

->P8a

=P8a
#BG: CuartoMuroi2
#Video: Investigación
...
...

#Interfaz: Diario

“Esto es surrealista. Tiene que ser mentira, ¿no?” #speaker: Izai #portrait: Izai_Sorpresa #audio: Miedo
“Pero a la vez… Es tan detallado y cuadra todo tan bien… Las fechas… Justo el día 10 se anunció la desaparición de Chris, y esta página es del día 9…” #portrait: Izai_Preocu
“No quiero creerlo pero… ¿y si la siguiente soy yo? Nadie me creería. ¿Qué puedo hacer ahora?” 

*[Volver al cuarto.] #speaker: Izai 
“Mejor vuelvo a la cama y me olvido de todo” 
->P8b
*[Escribir en el diario.] #speaker: Izai
#Interfaz: Diario2
“Así al menos, si alguien vuelve a llegar hasta aquí, podrá creerse lo que dice el diario. Voy a guardarlo en el cajón para que nadie lo vea sin querer” #speaker: Izai #portrait: Izai_Neutral
->P8b

=P8b
“Aunque ahora que lo pienso… El diario estaba encima de la mesa, así que no es muy difícil de ver… Y en este cuarto está Akil solo desde que no está Chris…” #speaker: Izai #portrait: Izai_Preocu
“Eso significa que ha tenido que leerlo sí o sí. Entonces… ¿por qué no ha dicho nada…? No tiene sentido, a no ser que él… No, imposible. Akil es mi amigo, él no…”
... #portrait: Izai_Triste
¿Qué hace aquí a estas horas, señorita Izai? #speaker: Doña Piedad #portrait: Piedad_Psico
¡! #speaker: Izai #portrait: Izai_Sorpresa

#Escena: GOOD


->DONE