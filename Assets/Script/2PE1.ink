INCLUDE 2PEA
INCLUDE 2PEB

->Intro

==Intro==
#BG: ClaseMuroi1
#audio: Neutral

¿Os ha quedado claro esto? #speaker: Doña Piedad #portrait: Piedad_Neutral
Sii #speaker: Todos  #portrait: Todos
zzzz #speaker: Zaida #portrait: Zaida2_Neutral
Espero que sea verdad porque es muy importante de cara al examen. En fin, pues si todo está claro, ha terminado la clase por hoy. #speaker: Doña Piedad 

PIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII #speaker: Timbre #portrait: Todos #efecto: timbre

(se despierta) ¿Qué? ¿Eh? ¿Qué ha pasado? #speaker: Zaida #portrait: Zaida2_Sorpresa
*Buenos días, bella durmiente. ->Introa #speaker: Izai #portrait: Izai2_Feliz
*Serás vaga… ->Introb #speaker: Izai #portrait: Izai2_Feliz

=Introa
 Pasa que ha terminado la clase.
Oh por Dios, por fin, me estaba muriendo de aburrimiento ya. #speaker: Zaida #portrait: Zaida2_Neutral
¿Qué sueño has tenido como para aburrirte dormida? #speaker: Izai #portrait: Izai2_Feliz
->Introc

=Introb
 Te has pasado toda la clase dormida.
Sí, pero creo que ha sido contraproducente (bosteza) #speaker: Zaida #portrait: Zaida2_Neutral
¿Y eso? #speaker: Izai #portrait: Izai2_Neutral
->Introc

=Introc
Soñé con clases particulares de Piedad sobre la importancia de los mitos. #speaker: Zaida #portrait: Zaida2_Neutral
Eso es justo lo que hemos dado hoy en clase. #speaker: Walid #portrait: Walid2_Neutral
¿En serio? #speaker: Zaida #portrait: Zaida2_Sorpresa
(bozteza) Por desgracia si. #speaker: Haro #portrait: Haro2_Preocu
La mente es maravillosa… #speaker: Zaida #portrait: Zaida2_Feliz
Antes de iros recordad que para el próximo día tenéis que hacerme el trabajo de algún rito, tradición o comportamiento humano que os resulte interesante. Será un trabajo evaluable que tendréis que presentar. #speaker: Doña Piedad #portrait: Piedad_Neutral
No… ¿Por qué otro puto trabajo más? ¿Es que no podemos descansar ni un día? #speaker: Haro #portrait: Haro2_Enfado
Pero si tu no los haces nunca #speaker: Walid #portrait: Walid2_Feliz
¿Y? Tú tampoco es que hagas muchos. #speaker: Haro #portrait: Haro2_Enfado
Hago los que considero más importantes. #speaker: Walid #portrait: Walid2_Neutral
Yo también, por eso no hago ninguno. #speaker: Haro #portrait: Haro2_Feliz
Pero si Walid se esfuerza un montón, ¿qué dices? #speaker: Zaida #portrait: Zaida2_Preocu

*[Callarte] ->Introd #speaker: Izai #portrait: Todos
*[Decir lo que piensas] ->Introe

=Introd
… #speaker: Izai #portrait: Izai2_Neutral
Si no digo que no, solo que no tiene nada que decirme a mi. #speaker: Haro #portrait: Haro2_Preocu
Vale, vale, callate ya bufón, tenemos que irnos a la siguiente clase. #speaker: Walid #portrait: Walid2_Preocu
¿Quieres pelea, friki? Porque te advierto que en el vida real no hay que pulsar un botón y yo tengo más calle que tú. #speaker: Haro #portrait: Haro2_Enfado
Por Dios… Izai, ¿has entendido algo de lo que han dicho? Porque en los últimos segundos solo he entendido unga unga unga. #speaker: Zaida #portrait: Zaida2_Preocu
(Se ríe) Yo tampoco les he entendido, no. Vámonos a clase de una vez. #speaker: Izai #portrait: Izai2_Feliz
->P1

=Introe
La diferencia entre vosotros es que Walid hace lo que puede cuidando de su salud mental y que a ti simplemente todo te da igual porque tienes la vida resuelta y no tienes gusto por nada. #speaker: Izai #portrait: Izai2_Enfado
¡Pues…! No te voy a decir que no, aunque si hay algo que me gusta. #speaker: Haro #portrait: Haro2_Triste
¿Ah, si? ¿El qué? #speaker: Zaida #portrait: Zaida2_Feliz
Ver el fútbol con mi padre… En mi restaurante de pizza favorito de Italia. #speaker: Haro #portrait: Haro2_Neutral
Wow. Que específico. #speaker: Walid #portrait: Walid2_Feliz
Así soy yo. #speaker: Haro #portrait: Haro2_Feliz
Eso no tiene… Arg, da igual. Vámonos a clase. #speaker: Izai #portrait: Izai2_Neutral
->P1

==P1==

#BG: Clase
Buenos días clase, venga, un último achuchón más hoy para poder descansar un rato. #speaker: Don Zack #portrait: Zack
Nunca entenderé la positividad de este tío. #speaker: Haro #portrait: Haro2_Neutral
Por un profesor amable que hay no te quejes. #speaker: Zaida #portrait: Zaida2_Preocu
Como os veo un poco cansados hoy, quiero que me hagáis un dibujo libre pero utilizando la técnica que vimos el otro día: el puntillismo. #speaker: Don Zack #portrait: Zack
¿Si hago cuatro cuadrados con puntos le valdrá? #speaker: Haro #portrait: Haro2_Feliz
Si tienes una intención comunicativa, todo vale. #speaker: Zaida #portrait: Zaida2_Neutral
Mucho texto. #speaker: Haro #portrait: Haro2_Feliz
Voy a pasar lista, podéis empezar. #speaker: Don Zack #portrait: Zack
Creo que voy a reutilizar uno de mis dibujos de ángeles para esto… #speaker: Zaida #portrait: Zaida2_Feliz
Oye, Zaida. ¿Te has dado cuenta de que hoy ha vuelto a faltar Karen? #speaker: Haro #portrait: Haro2_Preocu
¿Quien? #speaker: Zaida #portrait: Zaida2_Neutral
¿Estás de broma? La gemela rubia buenorra que se sienta en primera fila. #speaker: Haro #portrait: Haro2_Neutral
¡Ah! La chica que se suele encargar de los materiales. La verdad es que si que es muy mona y tiene un estilazo que cualquier tienda de mi madre envidiaria… #speaker: Zaida #portrait: Zaida2_Feliz
Sí, bueno, pues lleva un par de días sin venir. ¿Y si ha desaparecido? #speaker: Haro #portrait: Haro2_Neutral
¿En serio? ¿Ahora tú también? De Walid me lo esperaría pero, ¿tú? #speaker: Zaida #portrait: Zaida2_Neutral
A ver, no es que haya 'desaparecido', solo digo que no está. #speaker: Haro #portrait: Haro2_Neutral
No lo había notado… Más bien creo que nadie la echa en falta. #speaker: Zaida #portrait: Zaida2_Neutral
Hasta que sale en la revista. #speaker: Haro #portrait: Haro2_Neutral
¿Quien sabe? A lo mejor una demonio de la venganza se la ha llevado al infierno. #speaker: Zaida #portrait: Zaida2_Feliz
Y dale con las monas chinas… #speaker: Haro #portrait: Haro2_Preocu
Si no quieres mi opinión no me la preguntes. #speaker: Zaida #portrait: Zaida2_Enfado
Y ahora se ofende… #speaker: Haro #portrait: Haro2_Neutral
Ey, chicos, ¿qué tal? ¿Sabéis que vais a hacer ya para el trabajo? #speaker: Don Zack #portrait: Zack
Arte abstracto. #speaker: Haro #portrait: HAro2_Feliz
Estoy deseando verlo. #speaker: Don Zack #portrait: Zack
Tengo una idea en mente aunque no sé si tiene mucho sentido. #speaker: Zaida #portrait: Zaida2_Neutral
Inténtalo y dáselo, confío en tí Zaida, tienes un don para esto. #speaker: Don Zack #portrait: Zack
Lo haré. #speaker: Zaida #portrait: Zaida2_Feliz
Pelota. #speaker: Haro #portrait: Haro2_Enfado
Cállate ya, pesado. #speaker: Zaida #portrait: Zaida2_Enfado

->P2

==P2==
#BG: Patio
#audio: Feliz

¡Hola Izai! #speaker: Zaida #portrait: Zaida2_Feliz
¿Ya tienes la revista? #speaker: Walid #portrait: Walid2_Feliz

*[Contarles que participas en la revista] ->P2a #speaker: Izai #portrait: Izai2_Neutral
*[No decirselo.] ->P2b

=P2a
¡Hola! Mirad, participo en la revista. #speaker: Izai #portrait: Izai2_Feliz
¿Esto es lo que tenías que hacer ayer? #speaker: Walid #portrait: Walid2_Neutral
Si. Todos los días a 4º hora. #speaker: Izai #portrait: Izai2_Feliz
¡Qué guay! Me alegro por ti. Aunque no sabía que te interesara el periodismo. #speaker: Zaida #portrait: Zaida2_Feliz
->P2c

=P2b
Si, mira, toma. #speaker: Izai #portrait: Izai2_Neutral
¿Y esto? #speaker: Zaida #portrait: Zaida2_Sorpresa
¿Has entrevistado a Akil? #speaker: Walid #portrait: Walid2_Sorpresa
¿Desde cuando trabajas en esto? #speaker: Zaida #portrait: Zaida2_Neutral
Ayer fui a hablar con Akil por el anuncio de la vacante y estoy en periodo de prueba. #speaker: Izai #portrait: Izai2_Neutral
Ya hay que tener ganas de ponerse aún más trabajo encima. #speaker: Haro #portrait: Haro2_Neutral
->P2c

=P2c
Quería intentar averiguar más sobre las desapariciones, pero ayer lo único que saqué en claro es que de momento no voy a poder saber nada. #speaker: Izai #portrait: Izai2_Neutral
Pues vaya tontería, como si fueras a encontrar algo que no existe… #speaker: Haro #portrait: Haro2_Feliz
A ver si pone algo sobre ello hoy… #speaker: Izai #portrait: Izai2_Preocu

//transición interfaz revista
#audio: Null
//Interfaz de la revista
. #portrait: Todos
#BG: Transición

. #Revista: Revista2a
. #Revista: Revista2b
. #Revista: Revista2c
. #Revista: Revista2d
. #Revista: Revista2e
. #Revista: Revista2f
. #Revista: Revista2g

#BG: Patio

Esto es grave… 
Ya, ha desaparecido Karen, ¿no? #speaker: Zaida #portrait: Zaida2_Feliz
No solo ella. #speaker: Izai #portrait: Izai2_Neutral
¿Qué?  #speaker: Haro #portrait: Haro2_Preocu
Su gemela, Patricia, también. #speaker: Izai #portrait: Izai2_Preocu
… #speaker: Zaida #portrait: Zaida2_Preocu
… #speaker: Haro #portrait: Haro2_Preocu
Empiezo a sospechar que es verdad, que algo pasa. #speaker: Walid #portrait: Walid2_Neutral
Dos un mismo día…  #speaker: Zaida #portrait: Zaida2_Neutral
Seguramente hay alguien que descubrió una salida y lo ha empezado a decir por ahí.  #speaker: Haro #portrait: Haro2_Feliz
Si fuera así la gente se iría en masa o intentarían disimularlo escapando solo por la noche de fiesta o algo. #speaker: Izai #portrait: Izai2_Enfado
¿Y qué va a ser si no? #speaker: Haro #portrait: Haro2_Neutral
Hay muchas posibilidades… que se escapen, que los secuestren, que los maten, que los vendan, …  #speaker: Walid #portrait: Walid2_Preocu
JAJAJAJAJ #speaker: Haro #portrait: Haro2_Feliz
No te pases tampoco, Walid… #speaker: Zaida #portrait: Zaida2_Feliz
La única lógica es la primera. ¿Quién los van a secuestrar o matar o… JAJAJA ¿los van a vender? #speaker: Haro #portrait: Haro2_Feliz
Pueden secuestrarlos los profesores. #speaker: Zaida #portrait: Zaida2_Feliz
Pueden matarlos un alumno psicópata. #speaker: Izai #portrait: Izai2_Preocu
Estoy rodeado de locos…  #speaker: Haro #portrait: Haro2_Preocu
Pueden venderlos al gobierno. #speaker: Walid #portrait: Walid2_Neutral
Mi padre trabajó en el gobierno y nunca escuché nada de ventas de adolescentes ricos por ahí. #speaker: Haro #portrait: Haro2_Neutral
¿De verdad crees que tu padre te cuenta todo lo que pasa por los altos cargos? Yo estoy muy seguro de todo lo que me oculta mi abuelo porque lo tuve que averiguar yo. #speaker: Walid #portrait: Walid2_Feliz
Mi padre no me mentiría. #speaker: Haro #portrait: Haro2_Enfado
Ocultar información no es mentir. Eso es algo que tuve que descubrir a las malas. #speaker: Walid #portrait: Walid2_Neutral
Relajarse, esa no es la cuestión ahora. #speaker: Zaida #portrait: Zaida2_Enfado
Exacto. Además, que este sea un poco paranoico no significa que no esté pasando nada grave aquí. Y que no tenga razón en muchas ocasiones. #speaker: Izai #portrait: Izai2_Feliz
¡Oye! #speaker: Walid #portrait: Walid2_Enfado
Nuestras teorías son posibles. #speaker: Zaida #portrait: Zaida2_Enfado
Claro, claro. Porque los profesores o los alumnos psicópatas son algo muy normal. #speaker: Haro #portrait: Haro2_Feliz
¿Tú has visto Estados Unidos? #speaker: Zaida #portrait: Zaida2_Neutral
Sí, pero estamos en España, y aquí las armas no son legales ni estamos tan mal como los gringos. #speaker: Haro #portrait: Haro2_Neutral
Pues hasta que no me enseñes por dónde se escapan no creeré ni una ni otra. #speaker: Zaida #portrait: Zaida2_Neutral
La buscaré, no te preocupes, pero para entonces me largaré yo solo. Cuando salga en “desaparecidos” sabréis dónde estoy: en mi casa tranquilito.  #speaker: Haro #portrait: Haro2_Feliz
*[Pues vete ya.] ->P2d #speaker: Izai #portrait: Izai2_Neutral
*[¿Y si estás en peligro?] ->P2e

=P2d
En ese caso lárgate ya y déjanos tranquilos, pesado. #portrait: Izai2_Enfado
¿A mí me vas a echar? Vete tú a tu país. #speaker: Haro #portrait: Haro2_Enfado
Soy Granaina, gilipollas. #speaker: Izai #portrait: Izai2_Enfado
La árabe soy yo… #speaker: Zaida #portrait: Zaida2_Triste
Es verdad, es que como no te pones el pañuelo a veces se me olvida. #speaker: Haro #portrait: Haro2_Neutral
Que no lleve el hiyab por elección propia no significa que no sea musulmana y mucho menos árabe. #speaker: Zaida #portrait: Zaida2_Enfado
Que si, que si, que vale. Me da igual. #speaker: Haro #portrait: Haro2_Preocu
->P2f

=P2e
A lo mejor es que desapareces de verdad y te ha pasado algo malo. ¿Te gustaría que nos quedaramos sin hacer nada mientras estás en peligro? Tendríamos que investigar, juntos. #speaker: Izai #portrait: Izai2_Neutral
Que si, que si. #speaker: Haro #portrait: Haro2_Enfado
->P2f

=P2f
Vale, vale. Haya paz. #speaker: Walid #portrait: Walid2_Feliz

#audio: Null
 PIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII #speaker: Timbre #portrait: Todos #efecto: timbre

Vamos, que toca inglés. #speaker: Zaida #portrait: Zaida2_Neutral
Yo me tengo que ir con Akil. #speaker: Izai #portrait: Izai2_Feliz
¡Ánimo! #speaker: Zaida #portrait: Zaida2_Feliz
->P3

==P3==
#BG: PasilloMuroi
#audio: Neutral2

Anda, hola. Llegas pronto.  #speaker: Akil #portrait: Akil_Feliz
Siempre puntual.  #speaker: Izai #portrait: Izai2_Feliz
Entonces recuerdas lo que tenemos que hacer hoy, ¿no?  #speaker: Akil #portrait: Akil_Neutral

*[Entrevistar alumnos] ->P3a #speaker: Izai #portrait: Izai2_Neutral
*[Entrevistar al director] ->P3b #speaker: Izai #portrait: Izai2_Neutral

=P3a
Íbamos a hacer una entrevista a varios alumnos sobre el tema de las desapariciones. 
Es un buena idea, pero no. Hoy teníamos que entrevistar al director sobre su trayectoria en el instituto. La próxima vez haz los deberes.  #speaker: Akil #portrait: Akil_Neutral
¡Es verdad! Perdona #speaker: Izai #portrait: Izai2_Neutral
->P3c

=P3b
Vamos a entrevistar al director sobre su trayectoria en el San Muroi.  
Bingo. #speaker: Akil #portrait: Akil_Feliz
->P3c

=P3c
Tengo algunas preguntas preparadas. Como es tu primera entrevista oficial prefiero que observes un poco y si tienes alguna buena idea puedes participar. Aunque ten en cuenta que aún estás a prueba. #speaker: Akil #portrait: Akil_Neutral
Si, gracias, lo tendré en cuenta. #speaker: Izai  #portrait: Izai2_Neutral
Perfecto. Entremos, entonces. (toca a la puerta) #speaker: Akil #portrait: Akil_Neutral
->P3d

==P3d==

#BG: Despacho

Hola, Akil, pasa, pasa. #speaker: Don Andrés #portrait: Andres_Feliz
Gracias por recibirnos, director Andres. #speaker: Akil #portrait: Akil_Neutral
Siempre es un placer hablar con un joven tan capaz. Vaya… Veo que ya tienes nuevo ayudante, eh. #speaker: Don Andrés #portrait: Andres_Neutral
Si, hola, soy…  #speaker: Izai #portrait: Izai2_Feliz
Izai González, la becada de este curso ni más ni menos. Los profesores estamos muy contentos con tus resultados, creo que has elegido bien, Akil, como siempre. #speaker: Don Andrés #portrait: Andres_feliz
Aún está a prueba pero yo también lo creo. #speaker: Akil #portrait: Akil_Feliz
No sé qué decir… gracias. Aunque no hemos venido aquí a hablar de mí, ¿no? #speaker: Izai #portrait: Izai2_Feliz
Directa al grano, me gusta. Adelante, preguntadme lo que sea. #speaker: Dan Andrés #portrait: Andres_Feliz
Todos los alumnos sabemos que empezaste siendo profesor de matemáticas aquí en cuanto te graduaste con los máximos honores. Pero, ¿cómo llegaste a ser el director? #speaker: Akil  #portrait: Akil_Neutral
Con mucho esfuerzo y dedicación a la escuela. #speaker: Don Andrés #portrait: Andres_Neutral
Como sabéis para que te elijan, y recalco esta palabra, como director tiene que haber un consenso por parte de los profesores y directivos de la escuela. 
Siempre me han gustado los niños, así que me esfuerzo al máximo para atender todas las necesidades de mis alumnos, intento ser más un amigo que un profesor al que le tengan miedo. 
Con ello y mis innovadoras ideas sobre el centro y sus posibilidades, como el sistema de becas, mis compañeros me escogieron cuando llegó el momento de que mi madre se retirara.

*¿Qué opinión guarda sobre la anterior directora? #speaker: Izai #portrait: Izai2_Neutral
->P3e
*¿Entonces te escogieron porque tu madre era la directora antes?
->P3f

=P3e
La directora y fundadora del San Muroi, Ana Lucia, hizo una labor excelente y dejó el listón tan alto que dudo que nadie nunca pueda superarla. #speaker: Don Andrés #portrait: Andres_Neutral
Cambió el edificio y la reputación de este lugar por completo. Pasó de ser el reformatorio más temido del país a ser una escuela referente en el extrenjero. Le debemos todo lo que tenemos hoy.
Se nota que fue un gran referente para esta escuela, y siempre lo será. #speaker: Izai #portrait: Izai2_Feliz
->P3g

=P3f
¿Entonces te escogieron porque tu madre era la directora antes? #speaker: Izai #portrait: Izai2_Neutral
¡Izai! #speaker: Akil #portrait: Akil_Enfado
Me escogieron por mis cualidades como director, que la fundadora sea mi madre no es relevante para nada. #speaker: Don Andrés #portrait: Andres_Enfado
Lo siento… #speaker: Izai #portrait: Izai2_Triste
->P3g

=P3g
¿Cuál considera que es el aspecto más tedioso de dirigir el centro? #speaker: Akil #portrait: Akil_Neutral
El papeleo, sin duda. Hay cientos de papeles que firmar cada día y puede llegar a ser agotador. #speaker: Don Andrés #portrait: Andres_Neutral
(se ríe) Entendible, la verdad. Supongo que al final a ninguno nos gusta el tiempo que consume esas gestiones. ¿Y lo que más disfrutas? #speaker: Akil #portrait: Akil_Neutral
Aunque parezca algo contradictorio, me gusta mucho organizar la jerarquía del internado. Me explico. #speaker: Don Andrés #portrait: Andres_Neutral
Este lugar es como un reloj, todos los componentes deben funcionar a la perfección para poder mantenerse en la excelencia, y yo soy el que tiene que ordenar los componentes para que den lo mejor de sí. 
Es satisfactorio, y más cuando no se trata de un reloj, si no de un lugar destinado para que los jóvenes aprendan y crezcan como personas para ser la mejor versión de sí mismos en la vida adulta.

*[Aunque en realidad luego siguen siendo unos críos inmaduros] #speaker: Izai #portrait: Izai2_Neutral
->P3h
*Eso es muy loable por su parte. #speaker: Izai #portrait: Izai2_Neutral
->P3i

=P3h
Aunque en el fondo siguen siendo unos mimados que tienen la vida resuelta.
Puede ser, pero la intención es lo que cuenta, ¿no? #speaker: Don Andrés #portrait: Andres_Neutral
Sí, claro. #speaker: Izai #portrait: Izai2_Neutral
->P3j

=P3i
Eso es muy loable por su parte. #speaker: Izai #portrait: Izai2_Feliz
Que va, solo hago mi trabajo. Que me encanta, por cierto. #speaker: Don Andrés #portrait: Andres_Feliz
->P3j

=P3j
También quería hacer hincapié en un tema que ya mencionó usted antes por casualidad. #speaker: Akil #portrait: Akil_Neutral
Adelante. #speaker: Don Andrés #portrait: Andres_Neutral
Entre otras muchas cosas, se le conoce por ser el que promovió el sistema de becas… En fin, ¿cómo se le ocurrió? #speaker: Akil #portrait: Akil_Neutral
Pues tengo una anécdota con respecto a ello. #speaker: Don Andrés #portrait: Andres_Neutral
Un día en las vacaciones de verano salí a pasear con mi marido y mi hijo por el parque, y el pequeño se hizo amigo de una niña que estaba jugando por allí. Su nombre era Celia y parecía estar perdida, así que me acerqué a ella y le pregunté por su familia.
Su madre se había suicidado hacía dos años y a su padre nunca lo conoció. Estaba sola y sobrevivía sola como podía. 
Ese día reaccioné y vi lo diferentes que eran mi hijo, que siempre ha tenido todo lo que ha necesitado y esa niña del parque, cuando en realidad son iguales. A la niña la adoptamos, fue un proceso largo pero estábamos decididos a darle un futuro mejor.  
Aparte de participar en organizaciones benéficas, sentía que no hacía lo suficiente por los que no han tenido la suerte que yo sí he tenido, así que quise también mi pequeño grano de arena en la escuela y propuse hacer el sistema de becas. 
Aunque me parecía insuficiente, era algo con lo que empezar, y si todo sale bien el año que viene aumentará en número de becados: mínimo dos por curso.

*[Hipócrita.] #speaker: Izai #portrait: Izai2_Neutral
->P4
*[Eso es un gran avance.]
->P5









->DONE
