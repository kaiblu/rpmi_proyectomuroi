-> Intro

==Intro==
#BG: CuartoMuroi

Izai… Levanta… ¿Has vuelto a quedarte dormida leyendo? #speaker: Zaida #portrait: Zaida3_Preocu
¿Izai?  #portrait: Zaida3_Sorpresa
“No está en la cama. ¿Dónde se ha metido?” #portrait: Zaida3_Neutral
“¿Ha decidido esperar casi a final de curso para empezar a levantarse temprano?”
“Bueno, espera. Se ha dejado el móvil. Eso sí que es raro…”
“En fin ya la veré en clase…”
 ->P1

==P1==
#BG: ClaseMuroi1

Buenos días, Zaida. #speaker: Walid #portrait: Walid2_Neutral
Hola… (bosteza) #speaker: Haro #portrait: Haro2_Preocu
Hola, chicos. ¿Habéis visto a Izai? #speaker: Zaida #portrait: Zaida2_Preocu
Que va, siempre llegáis a la vez. ¿Por? #speaker: Walid #portrait: Walid2_Neutral
No estaba en el cuarto cuando me desperté. Pensé que estaría en clase pero aquí tampoco está… #speaker: Zaida #portrait:Zaida2_Triste
Lo más raro es que su uniforme, el móvil y la mochila estaban allí… No la encuentro.
Esa empollona no faltaría nunca a clase. ¿A lo mejor está enferma y fue a la enfermería? #speaker: Haro #portrait: Haro2_Neutral
Vengo de allí y tampoco la han visto… #speaker: Zaida #portrait: Zaida2_Triste
¿No creeréis que…? #portrait: Zaida2_Preocu
¡Eso es imposible! Nos querrá gastar alguna broma… #speaker: Haro #portrait: Haro2_Feliz
¿Izai? ¿Gastándonos una broma así? #speaker: Zaida #portrait: Zaida2_Enfado
Ya, bueno, lo veo más probable que eso. #speaker: Haro #portrait: Haro2_Enfado
Porque es imposible que haya desaparecido, ¿verdad? #portrait: Haro2_Triste
… #speaker: Walid #portrait: Walid2_Preocu
Izai nunca se habría ‘pirado’ del centro… 
… #speaker: Zaida #portrait: Zaida2_Triste
… #speaker: Haro #portrait: Haro2_Triste
… #speaker: Walid #portrait: Walid2_Triste
A lo mejor  sí que está pasando algo grave aquí dentro… #portrait: Walid2_Neutral
¿Crees que está…? #speaker: Zaida #portrait: Zaida2_Triste
¡NO! #speaker: Haro #portrait: Haro2_Enfado
Eso es imposible… #portrait: haro_Preocu
Tenemos que encontrarla. Sea como sea. #portrait: Haro2_Neutral

->P2

==P2==
#BG: Transición
#Video: Sotano

->P3

==P3==
#BG: Sotano
¿Por qué…? #speaker: Izai #portrait: Izai_Muerta
¿Por qué me haces esto…?
Lo siento… Era la única forma de engañarla. #speaker: ??? #portrait: Todos
Déjame salir… #speaker: Izai #portrait: Izai_Muerta
Pensé que éramos amigos.
Akil. #portrait: Todos

#Video: Epilogo
.
.
.
.
#BG: Transición
#Escena: Menu


->DONE
