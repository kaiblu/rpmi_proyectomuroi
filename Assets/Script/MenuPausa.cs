using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuPausa : MonoBehaviour
{
    public static bool GameIsPaused = false;

    public GameObject pauseMenuUI;
   

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("pausa");
            if (GameIsPaused)
            {

                Resume();

            } else
            {

                Paused();

            }

        }

    }
    public void Resume()
    {

        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;

    }

     void Paused()
    {

        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;

    }

    public void LoadMenu()
    {

        SceneManager.LoadScene("Menu");

    }

    public void QuitGame ()
    {

        Application.Quit();

    }

}
